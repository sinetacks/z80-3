	org $ 8000

offscreen equ $c000 ; offscreen drawing area

	jp start

sprite_map
	ds 640 , 0


;#####################

clear_map 
	ld hl,sprite_map
	ld de,sprite_map+1
	ld bc, 639 ; one less than the map size
	ld (hl),0
	ldir
	ret

clear_char

	push af
;	push hl
;	ld b,8
;cc_loop
;	ld (hl),a
;	inc h
;	djnz cc_loop
;	pop hl
	exx
; a is always zero
	ld (de),a
	res 7,d
	ld (de),a
	set 7,d
	exx
	pop af
	ret

back_char


	push hl
	push de
	push hl
	pop de
	ld a,d
	or $80
	ld d,a
	ld b,8
bd_loop
	ld a,(de)
	ld (hl),a
	inc d
	inc h
	djnz bd_loop

	exx
	push de
	pop hl
	res 7,h
	ld a,(de)
	ld (hl),a
	exx
	pop de
	pop hl
	ret

;###############

refresh


	ld hl,$4000
	ld de,sprite_map
	exx
	ld de,$D800
	exx
	ld bc,640

	halt

ref_loop
	push bc
	push de
	push hl

	ld a,(de)
	and a ; is it zero
	call z,clear_char
	call nz,back_char

	pop hl
	inc hl
	ld a,l
	and a
	jr nz,sameseg
	bit 0,h
	jr z, sameseg
	
	ld a,h
	dec a
	add a,8
	ld h,a

sameseg
	pop de
	inc de
	exx
	inc de
	exx

	pop bc
	dec bc
	ld a,b
	or c
	jr nz, ref_loop


	ret


;########################
;sprite call jump table

sprite_calls
	dw safe_sprite
	dw sprite_none
	dw sprite_none
	dw sprite_none
	dw clipped_sprite
	

sprite_routine

	ld a,(hl)
	inc hl
	ld h,(hl)
	ld l,a
	jp (hl)

;############
safe_previous

	ld hl,sprite_map
	add hl,de
	exx
	ld bc,0
	exx
	ld bc,$0404
sp_down
	push bc
	push hl
sp_along
	ld a,(hl)
	inc hl
	and a
	jr nz,sp_ok
	ccf
sp_ok
	exx
	rl c
	rl b
	exx
	dec c
	jr nz,sp_along
	pop hl
	ld bc,32
	add hl,bc
	pop bc
	djnz sp_down

	ret


safe_sprite
	
	ld a,(ix+0)
	ld l,a
	ld h,0
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	ld a,(ix+1)
	ld d,0
	ld e,a
	add hl,de
	ex de,hl ; de holds the offset into any 32*2- buffers

	push de

	call safe_previous

	pop de
	push de


here
	push de
	ld hl,sprite_map
	add hl,de 
 
	ex de,hl
;hl point to sprite_map location



	exx
	ld d,b
	ld e,c

	ld hl,$D800 ; attribs store
	pop bc
	add hl,bc


	push hl
	pop bc ; bc to attrib area

	push de
	pop hl
	exx

	ld bc,$0404
	ld h,(ix+5)
	ld l,(ix+4)
safe_map_loop
	push bc
	push de
	exx
	push bc
	exx
safe_map_loop2

	exx
	rl e
	rl d
	exx
	jr nc,safe_no
	ld a,(hl)
	and a
	jr z,safe_no

; this would be a good place to update the colour!!

	exx
	ld (bc),a
	exx

	ld a,(ix+8)
	ld (de),a
safe_no
	inc de
	inc hl
	exx
	inc bc
	exx

	dec c
	jr nz,safe_map_loop2

	exx
	pop bc
	push hl
	ld hl,32
	add hl,bc
	push hl
	pop bc
	pop hl
	exx

	pop de
	ex de,hl
	ld bc,32
	add hl,bc
	ex de,hl
	pop bc
	djnz safe_map_loop	
	
	pop de

	exx
	push hl
	pop de
	exx


	ld hl,$5800 ; attribs
	add hl,de ; this is the attib for the screen

	call attr2backdisp
	ld l,(ix+2)
	ld h,(ix+3)


	ld bc,$0404
sprite_down
	push bc
	push de
sprite_along
	push bc
	push de

	exx
	rl e
	rl d
	exx
	ld b,8
	jr c,sprite_pokes

	ld bc,8
	add hl,bc

	jr sprite_no

sprite_pokes
	ld a,(hl)
	ld (de),a
	inc hl
	inc d
	djnz sprite_pokes

sprite_no

	pop de
	inc de
	pop bc
	dec c
	jr nz,sprite_along
	pop de
	ex de,hl
	ld bc,32
	add hl,bc
	ex de,hl
	bit 0,d
	jr z, sameseg2
	
	ld a,d
	dec a
	add a,8
	ld d,a

sameseg2

	pop bc
	djnz sprite_down

sprite_none

	ret
	

attr2backdisp 
	xor a
	ld a,h
	and 3
	rla
	rla
	rla
	or $c0
	ld d,a
	ld e,l
	ret


;###############


vis_sprite
	
	exx
	ld hl,0
	exx
	ld hl,0

	ld d,(ix+1)
	ld e,(ix+0)

	ld b,4

vis_loop	
	push bc

	call xbounds
	call ybounds

	inc e
	inc d
	pop bc
	djnz vis_loop

	ld a,h
	and l
	rla
	rla
	rla
	rla
	ld b,a
	exx
	ld a,h
	and l
	exx
	or b	
	ld (clip_byte),a
	ret

clip_byte	db 0

minx equ 0
maxx equ 32
miny equ 0
maxy equ 20


xbounds


	ld b,minx
	ld c,maxx
	ld a,d
	cp b
	ccf
	rl h
	cp c
	rl l
	ret

ybounds

	ld b,miny
	ld c,maxy
	ld a,e
	cp b
	ccf
	exx
	rl h
	exx
	cp c
	exx
	rl l
	exx
	ret

;################


work db 0


bound_cells

	ld hl,work
	ld bc,$0000
	ld a,(clip_byte)
;	and a
;	ret z ; safe return for zero value
;	and $f0
;	ret z
;	ld a,(clip_byte)
;	and $0f
;	ret z
	ld (hl),a
	and $f0
	ld d,a
	rld
	ld a,(hl)
	and $f0
	ld e, a
top	rl d
	jr c,gott
	inc b
	jr top
gott	rl e
	jr c,gotl
	inc c
	jr gott

gotl

	ld a,(clip_byte)
	ld (hl),a
	and $0f
	ld e,a
	rrd
	ld a,(hl)
	and $0f
	ld d,a
	ld hl,$0303

bot	rr d
	jr c,gotb
	dec h
	jr bot
gotb	rr e
	jr c,gotr
	dec l
	jr gotb

gotr
;got all now

;	(c,b) top visible
;	(l,h) bot visible

	ret

;##############

draw_sprite

;hl points to sprite definition

	call vis_sprite

	ld hl,sprite_calls

	ld a,(clip_byte)
	cp $ff
	jp z,callme
	inc hl
	inc hl
	ld a,(clip_byte)
	cp 0
	jp z,callme
	inc hl
	inc hl
	ld a,(clip_byte)
	and $f0
	jp z,callme
	inc hl
	inc hl
	ld a,(clip_byte)
	and $0f
	jp z,callme
	inc hl
	inc hl
callme
	call sprite_routine
	ret

	
;#################

;#################

clipped_sprite 


	call bound_cells
	call selfmod_code
	call adj_sprite

	ret


;###########
selfmod_code

	ld a,l
	sub c
	inc a
	ld (adj_prev_dims+2),a
	ld (adj_map_dims+2),a
	ld (adj_attr_dims+2),a
	ld (adj_spr_dims+2),a
	ld a,h
	sub b
	inc a
	ld (adj_prev_dims+1),a
	ld (adj_map_dims+1),a
	ld (adj_attr_dims+1),a
	ld (adj_spr_dims+1),a
	ld a,c
	ld (adj_y+1),a
	ld a,b
	ld (adj_x+1),a
	ld a,c
	add a,a
	add a,a
	ld d,a
	add a,a
	add a,a
	add a,a
	ld e,a
	ld a,b
	add a,a
	add a,a
	add a,a
	add a,e
	ld (adj_spr_off+1),a
;	ld (attr_off+1),a
	ld a,d
	add a,b
	ld (adj_attr_off+1),a
	ret

;########################

;############
adj_previous

	ld hl,sprite_map
	add hl,de ; adjustment already done

	exx
	ld bc,0
	exx

adj_prev_dims
	ld bc,$0404
	push bc ; safe it for later

adjp_down
	push bc
	push hl
adjp_along
	ld a,(hl)
	inc hl
	and a
	jr nz,adjp_ok
	ccf
adjp_ok
	exx
	rl c
	rl b
	exx
	dec c
	jr nz,adjp_along
	pop hl
	ld bc,32
	add hl,bc
	pop bc
	djnz adjp_down

; need to add code to move correct num bytes left

	pop hl ; original bc now in hl 

	ld b,4	
	xor a
	rl l
	rl l
	rl l
	rl l

	
adj_mul rla
	rl l
	jr nc,adj_mul_skip
	add a,h
adj_mul_skip
	djnz adj_mul

	and a
	ret z
	neg
	add a,16
	ld b,a
	xor a
adj_mask
	exx
	rl c
	rl b
	exx
	djnz adj_mask
		
	ret

adj_sprite
	
adj_y	ld a,0
	add a,(ix+0)
	ld l,a

	ld h,0
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
	add hl,hl
adj_x
	ld a,0
	add a,(ix+1)
	ld d,0
	ld e,a
	add hl,de
	ex de,hl ; de holds the offset into any 32*2- buffers

	push de

	call adj_previous

	pop de
	push de


	ld hl,sprite_map
	add hl,de 
 
	ex de,hl
;hl point to sprite_map location

	exx
	ld a,(ix+7)
	and b
	ld h,a
	ld a,(ix+6)
	and c
	ld l,a
	push hl
	pop de
	exx

adj_map_dims

	ld bc,$0404
adj_map_loop
	push bc
	push de
adj_map_loop2

	exx
	rl e
	rl d
	exx
	jr nc,adj_no
	ld a,(ix+8)
	ld (de),a
adj_no
	inc de
	inc hl
	dec c
	jr nz,adj_map_loop2
	pop de
	ex de,hl
	ld bc,32
	add hl,bc
	ex de,hl
	pop bc
	djnz adj_map_loop	
	
	pop de
	push de

	exx
	push hl
	pop de
	exx

	ld hl,$D800
	add hl,de
	ex de,hl

	ld l,(ix+4)
	ld h,(ix+5)
adj_attr_off
	ld bc,0
	add hl,bc

adj_attr_dims
	ld bc,$0404
adj_attr_map_loop
	push bc
	push hl
	push de

adj_attr_map_loop2

	exx
	rl e
	rl d
	exx
	jr nc,adj_attr_no

	ld a,(hl)
	ld (de),a
adj_attr_no	inc de
	inc hl
	dec c
	jr nz,adj_attr_map_loop2

	pop de
	ex de,hl
	ld bc,32
	add hl,bc
	ex de,hl
	pop hl
	ld bc,4
	add hl,bc
	pop bc
	djnz adj_attr_map_loop	

	pop de
	exx
	push hl
	pop de
	exx
	
	ld hl,$5800 ; attribs
	add hl,de ; this is the attib for the screen

	call attr2backdisp
	ld l,(ix+2)
	ld h,(ix+3)
adj_spr_off
	ld bc,0
	add hl,bc

adj_spr_dims
	ld bc,$0404
adj_sprite_down
	push bc
	push hl
	push de
adj_sprite_along
	push bc
	push de

	exx
	rl e
	rl d
	exx
	ld b,8
	jr c,adj_sprite_pokes

	ld bc,8
	add hl,bc

	jr adj_sprite_no


adj_sprite_pokes
	ld a,(hl)
	ld (de),a
	inc hl
	inc d
	djnz adj_sprite_pokes

adj_sprite_no

	pop de
	inc de
	pop bc
	dec c
	jr nz,adj_sprite_along
	pop de
	ex de,hl
	ld bc,32
	add hl,bc
	ex de,hl
	bit 0,d
	jr z, adj_sameseg2
	
	ld a,d
	dec a
	add a,8
	ld d,a

adj_sameseg2

	pop hl
	ld bc,32
	add hl,bc
	pop bc
	djnz adj_sprite_down

	ret



;##############################################
;# TEST CALLING CODE AND DATA                 #
;# DEFINED SPRITES                            #
;#                                            #
;##############################################


pixel1_data

rept 2
	ds 32, $f0
	ds 32, $0f
endr

pixel2_data

rept 64
	db $aa, $55
endr

colour_data 
rept 3
	ds 3,%00000010
	db 0
endr
	ds 4,0

colour2_data 
	ds 16,%00000110



;#########
; sprites from ZX Paintbrush


triangle_pixels

; ASM data file from a ZX-Paintbrush picture with 32 x 32 pixels (= 4 x 4 characters)
; and an attribute area of 16 bytes

; block based output of pixel data - each block contains 8 x 8 pixels

defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $01, $01
defb $00, $00, $00, $00, $00, $80, $C0, $C0
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $03, $03, $07, $07, $0F, $0F, $1F, $1F
defb $E0, $E0, $F0, $F0, $F8, $FC, $FC, $FE
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $01, $01, $03
defb $3F, $7F, $7F, $FF, $FF, $FF, $FF, $FF
defb $FE, $FF, $FF, $FF, $FF, $FF, $FF, $FF
defb $00, $00, $80, $80, $C0, $C0, $E0, $E0
defb $03, $07, $07, $0F, $0F, $1F, $00, $00
defb $FF, $FF, $FF, $FF, $FF, $FF, $00, $00
defb $FF, $FF, $FF, $FF, $FF, $FF, $00, $00
defb $F0, $F8, $F8, $FC, $FC, $FE, $00, $00

triangle_attr

defb $00
defb $42
defb $42
defb $00
defb $00
defb $42
defb $42
defb $00
defb $42
defb $42
defb $42
defb $42
defb $42
defb $42
defb $42
defb $42


square_pixels

; ASM data file from a ZX-Paintbrush picture with 32 x 32 pixels (= 4 x 4 characters)
; and an attribute area of 16 bytes

; block based output of pixel data - each block contains 8 x 8 pixels

; blocks at pixel positionn (y=0):

defb $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
defb $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
defb $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
defb $00, $00, $00, $00, $00, $00, $00, $00

; blocks at pixel positionn (y=8):

defb $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
defb $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
defb $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
defb $00, $00, $00, $00, $00, $00, $00, $00

; blocks at pixel positionn (y=16):

defb $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
defb $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
defb $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FF
defb $00, $00, $00, $00, $00, $00, $00, $00

; blocks at pixel positionn (y=24):

defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00

square_attr
; attribute blocks at pixel position (y=0):

defb $47
defb $47
defb $47
defb $00

; attribute blocks at pixel position (y=8):

defb $47
defb $47
defb $47
defb $00

; attribute blocks at pixel position (y=16):

defb $47
defb $47
defb $47
defb $00

; attribute blocks at pixel position (y=24):

defb $00
defb $00
defb $00
defb $00


diamond_pixels

; ASM data file from a ZX-Paintbrush picture with 32 x 32 pixels (= 4 x 4 characters)
; and an attribute area of 16 bytes

; block based output of pixel data - each block contains 8 x 8 pixels

defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $01, $03, $07, $0F
defb $00, $00, $00, $80, $C0, $E0, $F0, $F8
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $01, $03, $07, $0F
defb $1F, $3F, $7F, $FF, $FF, $FF, $FF, $FF
defb $FC, $FE, $FF, $FF, $FF, $FF, $FF, $FF
defb $00, $00, $00, $80, $C0, $E0, $F0, $F8
defb $1F, $0F, $07, $03, $01, $00, $00, $00
defb $FF, $FF, $FF, $FF, $FF, $FF, $7F, $3F
defb $FF, $FF, $FF, $FF, $FF, $FF, $FF, $FE
defb $FC, $F8, $F0, $E0, $C0, $80, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $1F, $0F, $07, $03, $01, $00, $00, $00
defb $FC, $F8, $F0, $E0, $C0, $80, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00

diamond_attr

defb $00
defb $41
defb $41
defb $00
defb $41
defb $41
defb $41
defb $41
defb $41
defb $41
defb $41
defb $41
defb $00
defb $41
defb $41
defb $00


ring_pixels

; ASM data file from a ZX-Paintbrush picture with 32 x 32 pixels (= 4 x 4 characters)
; and an attribute area of 16 bytes

; block based output of pixel data - each block contains 8 x 8 pixels

; blocks at pixel positionn (y=0):

defb $00, $00, $00, $01, $07, $0F, $1E, $1C
defb $00, $00, $00, $C0, $F0, $F8, $3C, $1C
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $1C, $1C, $1F, $0F, $07, $01, $00, $00
defb $1C, $1C, $7C, $F8, $F0, $C0, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00
defb $00, $00, $00, $00, $00, $00, $00, $00

ring_attr

defb $44
defb $44
defb $00
defb $00
defb $44
defb $44
defb $00
defb $00
defb $00
defb $00
defb $00
defb $00
defb $00
defb $00
defb $00
defb $00

sprite1_def
	db 15,9
	dw square_pixels
	dw square_attr
	dw $ffff ; not used
	db 1

sprite2_def
	db 10,9
	dw triangle_pixels
	dw triangle_attr
	dw $ffff ; not used
	db 2

;##############

DISP_SPRITE	


;	call clear_map

	ld ix,sprite1_def
	call draw_sprite

	ld ix,sprite2_def
	call draw_sprite

	call refresh

	ret

;###################

blackout

	xor a
	inc a
	out (254),a
	ld hl,$5800
	ld de,$5801
	ld bc, ( 32 * 20) -1
	ld (hl),0
	ldir
	ld bc, (32 * 4) - 1
	ld (hl),%00001000
	ldir
	inc hl
	ld (hl),%00001000
	ret

;##########

seed db 100

sprite_table
	dw square_pixels
	dw square_attr

	dw diamond_pixels
	dw diamond_attr

	dw ring_pixels
	dw ring_attr

	dw triangle_pixels
	dw triangle_attr


rand

	 ld a, (seed)
	 ld b, a 
	 rrca ; multiply by 32
	 rrca
	 rrca
	 xor 0x1f
	 add a, b
	 sbc a, 255 ; carry
	 ld (seed), a
	 ret

rand_coord

	call rand
	and $1f
	dec a
	dec a
	ld (ix+1),a
	call rand
	and $1f
	dec a
	ld (ix+0),a

	call rand
	and 3
	ld l,a
	ld h,0
	add hl,hl
	add hl,hl
	ld de,sprite_table
	add hl,de
	ld a,(hl)
	ld (ix+2),a
	inc hl
	ld a,(hl)
	ld (ix+3),a
	inc hl
	ld a,(hl)
	ld (ix+4),a
	inc hl
	ld a,(hl)
	ld (ix+5),a
	inc hl
	ret

start	call blackout

	exx
	push hl
	push de
	exx

	call clear_map

anim_loop

	ld ix,sprite1_def
	call rand_coord
	ld ix,sprite2_def
	call rand_coord



	call disp_sprite
;	call anim

	ld bc,6000 ;6000
delay
	nop
	dec bc
	ld a,b
	or c
	jr nz, delay

	ld bc, 64510
	in a,(c)
	bit 0,a
	jr nz,anim_loop
      


	exx
	pop de
	pop hl
	exx

	ret
	
	


